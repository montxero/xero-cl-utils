(in-package :xero.utils)

(proclaim '(inline last1 singleton append1 ensure-list))

(defun last1 (list)
  "Return  the last element of list"
  (car (last list)))

(defun singleton (xs)
  "return T iff xs is a list of 1 item"
  (and (consp xs) (not (cdr xs))))

(defun append1 (list obj)
  "attach obj to the end of list non destructievly"
  (append list (list obj)))

(defun conc1 (list obj)
  "attach obj to the end of list destructievely"
  (nconc list (list obj)))

(defun ensure-list (obj)
  "returns obj if it is a list else, returns a list containing obj"
  (if (listp obj) obj (list obj)))

(defun longer (x y)
  "Retrun t iff x is longer than y."
  (labels ((compare (x y)
             (and (consp x)
                  (or (null y) (compare (cdr x) (cdr y))))))
    (if (and (listp x) (listp y))
        (compare x y)
        (> (length x) (length y)))))

(defun compose (&rest fns)
  "Return a new function wich is the result of composing fns.
Courtesy PG's onlisp and ansi commonlsip."
  (if fns
      (destructuring-bind (f1 . fs) (reverse fns)
        #'(lambda (&rest args)
            (reduce #'(lambda (v f) (funcall f v))
                    fs
                    :initial-value (apply f1 args))))
      #'identity))

(defun disjoin (predicate &rest predicates)
  "Return a function takes some argument and  returns true iff any of predicate or
predicates is true  returns true when applied to an argument"
  (if (null predicates)
      predicate
      (let ((disj (apply #'disjoin predicates)))
        #'(lambda (&rest args)
            (or (apply predicate args) (apply disj args))))))

(defun conjoin (predicate &rest predicates)
  "Return a function which takes some argument and returns true the argument is true for
predicate and all predicates."
  (if (null predicates)
      predicate
      (let ((conj (apply #'conjoin predicates)))
        #'(lambda (&rest args)
            (and (apply predicate args) (apply conj args))))))

(defun curry (fn &rest args)
  "Return a curried version of fn."
  #'(lambda (&rest args2)
      (apply fn (append args args2))))

(defun rcurry (fn &rest args)
  "Return the right curried version of fn."
  #'(lambda (&rest args2)
      (apply fn (append args2 args))))

(defun always (x)
  "Always return x."
  #'(lambda (&rest args)
      (declare (ignore args))
      x))

(defmacro dovector ((var vector &optional result) &body body)
  "The vector version of the dolist macro."
  (let ((v (gensym))
        (i (gensym)))
    `(let ((,v ,vector))
       (dotimes (,i (length ,v) ,result)
         (let ((,var (aref ,v ,i)))
           ,@body)))))

(defmacro dosequence ((var seq &optional result) &body body)
  "The dolist macro generalised to sequences."
  (let ((s (gensym)))
    `(let ((,s ,seq))
       (etypecase ,s
         (list (dolist (,var ,s ,result) (progn ,@body)))
         (vector (dovector (,var ,s ,result) (progn ,@body)))))))

(defmacro assert-nonnegative (n msg)
  (let ((g (gensym)))
    `(let ((,g ,n))
       (assert (and (integerp ,g) (not (minusp ,g)))
               (,g)
               ,msg))))

(defun take (n seq)
  "return a sequence containing the the first n elemets of seq.
The retruned sequence is of the same type as seq.
If n is larger than the length of seq, it should return a copy of seq."
  (assert-nonnegative n "Invalid length specifier. Can only take a nonnegative integer amount of elements from a sequence")
  (let ((i 0))
    (dosequence (x seq)
      (if (= i n)
          (return)
          (incf i)))
    (subseq seq 0 i)))

(defun drop (n seq)
  "return a subsequence of seq with the frist n elements of seq removed.
If n is larger than the length of seq, it should return an empty sequence of the same type as seq."
  (assert-nonnegative n "Invalid length specifier. Can only take a nonnegative integer amount of elements from a sequence")
  (let ((i 0))
    (dosequence (x seq)
      (if (= i n)
          (return)
          (incf i)))
    (copy-seq (subseq seq i))))

(defun take-while (predicate sequence)
  "Return the longest initial segement of sequence whose elements satisfy predicate.

Examples
--------
(take-while #'evenp '(2 4 6 8 9 14)) ;=> (2 4 6 8)
(take-while #'plusp ()) ;=> ()
(take-while #'plusp '(1 2 3 4 5)) ;=> (1 2 3 4 5)
(take-while #'plusp '(1 2 -9 3 4 5 6 7 8)) ;=> (1 2)
"
  (let ((result nil))
    (dosequence (x sequence)
      (if (funcall predicate x)
          (push x result)
          (return)))
    (setf result (nreverse result))
    (etypecase sequence
      (list result)
      (string (coerce result 'string))
      (vector (coerce result 'vector)))))

(defun drop-while (predicate sequence)
  "Retrun the result of dropping the longest initial subsequence
of sequence whose elements satisfy predicate

Examples
--------
(drop-while #'evenp '(2 4 6 8 9 14)) => (9 14)
(drop-while #'plusp ()) ;=> ()
(drop-while #'plusp '(1 2 3 4 5)) ;=> ()
(drop-while #'plusp '(1 2 -9 3 4 5 6 7 8)) ;=> (-9 3 4 5 6 7 8)
"
  (let ((n 0))
    (dosequence (x sequence n)
      (if (funcall predicate x)
          (incf n)
          (return n)))
    (drop n sequence)))

(defun group (source n)
  "return a list consisting of sublists source in groups of n starting from the first element.
The last sublist in the resulting list may have length less than n if n does not divide (length source).
"
  (labels ((rec (source acc)
             (if (emptyp source)
                 (nreverse acc)
                 (rec (drop n source) (cons (take n source) acc))))
           (emptyp (xs)
             (etypecase xs
               (list (null xs))
               (array (zerop (length xs))))))
    (unless (and (integerp n) (plusp n))
      (error "Invalid length. Lengths must be positive integers."))
    (rec source nil)))

(defun groupby (fn seq)
  "Return a hashtable where the keys are the set of values resulting from applying fn
to each element of seq and corresponding values are the list of elements which (fn element)
evaluates to key.

Examples
--------
(flet ((classify (c)
         (cond ((find c \"aeiou\") 'vowel)
               ((find c \",?.!:;\") 'punctuation)
               ((find c (list #\space #\newline #\tab)) 'whitespace)
               ((find c \"bcdfghjklmnpqrstvwxyz\") 'consonant)
               (t 'other))))))
  (groupby #'classify \"abcd efgh, ??!, wada? yaknow???'))
=>
{
VOWEL : (#\o #\a #\a #\a #\e #\a)
CONSONANT : (#\w #\n #\k #\y #\d #\w #\h #\g #\f #\d #\c #\b)
WHITESPACE : (#\  #\  #\  #\ )
PUNCTUATION : (#\? #\? #\? #\? #\, #\! #\? #\? #\,)
OTHER : (#\')
}

(groupby #'(lambda (n) (* n n)) '(-1 2 0 -2 -3 3 1))
=>
{
1 : (1 -1)
4 : (-2 2)
0 : (0)
9 : (3 -3)
}
"
  (let ((dict (make-hash-table :test #'equal)))
    (labels ((dict-insert (key value)
               (push value (gethash key dict))))
      (map nil
           #'(lambda (d) (dict-insert (funcall fn d) d))
           seq)
      dict)))

(defun print-hash-table (table stream)
  (format stream "~&{")
  (maphash #'(lambda (k v)
               (format stream "~&~S : ~S" k (or (and (hash-table-p v) (print-hash-table v nil)) v)))
           table)
  (format stream "~&}"))

(defmethod print-object ((table hash-table) stream)
  (format stream "~&{")
  (maphash #'(lambda (k v)
               (format stream "~&~S : ~S" k v))
           table)
  (format stream "~&}")  )

(defun seive (fn seq)
  "Return the list which results from applying fn to every element of seq keeping only true
values."
  (let ((result nil))
    (dosequence (s seq (nreverse result))
      (let ((x (funcall fn s)))
        (when x (push x result))))))

(defun flatten (tree)
  "Return a list of the leaves of tree in order of their occurence."
  (labels ((rec (xs acc)
             (cond ((null xs) acc)
                   ((atom xs) (cons xs acc))
                   (t (rec (car xs) (rec (cdr xs) acc))))))
    (rec tree nil)))

(defun prune (test tree)
  "Return a new tree where each elemet satisfying test is removed."
  (labels ((rec (tree acc)
             (cond ((null tree) (nreverse acc))
                   ((consp (car tree))
                    (rec (cdr tree)
                         (cons (rec (car tree) nil) acc)))
                   (t (rec (cdr tree)
                           (if (funcall test (car tree))
                               acc
                               (cons (car tree) acc)))))))
    (rec tree nil)))

(defun partition (predicate sequence)
  "Return a list of subsequnces of sequnce partioned at elements which are true for predicate.

Examples
--------
(partition #'evenp '(1 3 4 5 7 8 2 17 19)) ; => ((1 3) (5 7) NIL (17 19))
(partition #'plusp '(-1 -3 -5 0 2 -7 -9)) ; => ((-1 -3 -5 0) (-7 -9))
(partition #'(lambda (c) (char= #\space c)) \"Hello world\") ; => (\"Hello\" \"world\")
"
  (let ((result nil)
        (start 0)
        (end 0))
    (dosequence (x sequence (or (and (= start end) (nreverse result))
                                (nreverse (push (subseq sequence start end) result))))
      (cond ((funcall predicate x)
             (push (subseq sequence start end) result)
             (incf end)
             (setf start end))
            (t (incf end))))))

(defun mapsequences (function &rest sequences)
  "Return the list which results from mapping function over several sequences.
This is similar to mapcars described in onlisp. However, this function can work on a mixture of lists
and vectors."
  (let ((result nil))
    (dolist (xs sequences (nreverse result))
      (dosequence (obj xs)
        (push (funcall function obj) result)))))

(defun make-list-with-fresh-items (size &key initial-element)
  "Return a new list of length SIZE with each element a fresh and independent copy if INITIAL-ELEMENT.
This is inteneded to be a drop in replacement for MAKE_LIST

Example
-------
(let ((xs '(1 2 3)))
  (let ((ys (make-list-with-fresh-items 3 :initial-element xs)))
    (setf (caar ys) 0)
    ys))

=> ((0 2 3) (1 2 3) (1 2 3))

Contrast this with the builtin MAKE-LIST

(let ((xs '(1 2 3)))
  (let ((ys (make-list 3 :initial-element xs)))
    (setf (caar ys) 0)
    ys))

=> ((0 2 3) (0 2 3) (0 2 3))
"
  (let ((result (list)))
    (dotimes (i size result)
      (push (copy initial-element) result))))

(defgeneric copy (object)
  (:documentation "Return a copy of object"))

(defmethod copy ((xs list))
  (copy-list xs))

(defmethod copy ((xs vector))
  (copy-seq xs))

(defun mappend (fn &rest lsts)
  (apply #'append (apply #'mapcar fn lsts)))
