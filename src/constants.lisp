;;;; constants.lisp
;;;; 

(in-package #:xero.constants)

(defconstant +lowercase-english-alphabets+ "abcdefghijklmnopqrstuvwxyz" "Lower case English alphabets")
(defconstant +uppercase-english-alphabets+ "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "Upper case English alphabets")
