(defpackage #:xero.utils
  (:use #:cl)
  (:export
   :always
   :append1
   :compose
   :conc1
   :conjoin
   :curry
   :disjoin
   :dosequence
   :dovector
   :drop
   :drop-while
   :ensure-list
   :flatten
   :group
   :groupby
   :last1
   :longer
   :mapsequences
   :partition
   :prune
   :rcurry
   :seive
   :singleton
   :take
   :take-while
   :product
   :permutations
   :combinations
   :combinations-with-replacement))

(defpackage #:xero.constants
  (:use #:cl)
  (:export :+lowercase-english-alphabets+)
  (:export :+uppercase-english-alphabets+))
