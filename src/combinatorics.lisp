(in-package :xero.utils)

(defun iota (n)
  (loop for i from 0 below n collect i))

(defun product (&rest args)
  "Return the cartesian product of args"
  (flet ((product-2 (xs ys)
           (let ((result (list)))
             (dolist (x xs (nreverse result))
               (dolist (y ys)
                 (push (append (xero.utils:ensure-list x)
                               (xero.utils:ensure-list y))
                       result))))))
    (reduce #'product-2 (mapcar #'(lambda (x) (coerce x 'list)) args))))

(defun permutations (xs r)
  "Return successive r length permutations of elements in the sequence XS.
Adapted from Python itertools."
  (let ((result nil)
        (index-pool (remove-if
                     (complement #'(lambda (x) (= r (length (remove-duplicates x)))))
                     (apply #'product
                            (make-list-with-fresh-items r
                                                        :initial-element (iota (length xs)))))))
    (dolist (indicies index-pool (nreverse result))
      (let ((res nil))
        (dolist (i indicies)
          (push (elt xs i) res))
        (setf res (nreverse res))
        (typecase xs
          (string  (push (coerce res 'string) result))
          (vector  (push (coerce res 'vector) result))
          (t (push res result)))))))


(defun combinations (xs r)
"Return r length subsequences of elements from the sequence XS."
  (let ((result nil)
        (index-pool (remove-if (complement (lambda (x)
                                             (let ((y (sort (copy-seq x) #'<)))
                                               (every #'= x y))))
                               (permutations (iota (length xs)) r))))
    (dolist (indices index-pool (nreverse result))
      (let ((res nil))
        (dolist (i indices)
          (push (elt xs i) res))
        (setf res (nreverse res))
        (typecase xs
          (string  (push (coerce res 'string) result))
          (vector  (push (coerce res 'vector) result))
          (t (push res result)))))))

(defun combinations-with-replacement (xs r)
"Return r length subsequences of elements from the sequence XS allowing
individual elements to be repeated more than once."
  (let ((result nil)
        (index-pool (remove-if (complement (lambda (x)
                                             (let ((y (sort (copy-seq x) #'<)))
                                               (every #'= x y))))
                               (product (iota (length xs)) r))))
    (dolist (indices index-pool (nreverse result))
      (let ((res nil))
        (dolist (i indices)
          (push (elt xs i) res))
        (setf res (nreverse res))
        (typecase xs
          (string  (push (coerce res 'string) result))
          (vector  (push (coerce res 'vector) result))
          (t (push res result)))))))
