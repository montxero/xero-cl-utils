(asdf:defsystem #:xero
  :author "Sey MontXero"
  :description "Xero's personal Common lisp utilities"
  :depends-on nil
  :serial t
  :components ((:module "src"
                :serial t
                :components ((:file "packages")
                             (:file "utils")
                             (:file "constants")
                             (:file "combinatorics")))))
